<?php

namespace Drupal\Tests\drupal_test_assertions\Assertions;

use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Helper trait to perform assertions related to languages.
 */
trait LanguageTrait {

  /**
   * Asserts a language exists.
   */
  public function assertLanguageExists(string $language_name) {
    $this->assertIsObject(ConfigurableLanguage::load($language_name), "The language '$language_name' exists.");
  }

}
