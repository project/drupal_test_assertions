<?php

namespace Drupal\Tests\drupal_test_assertions\Assertions;

/**
 * Helper trait to do generic page assertions.
 */
trait PageTrait {

  /**
   * Asserts there is a specific metatag with a value in the current page.
   */
  public function assertMetatag(string $id, string $value) {
    $reg_exp = "/<meta.*property=\"$id\".*content=\"$value\".*\/>/";
    $this->assertMatchesRegularExpression($reg_exp, $this->getSession()->getPage()->getHtml(), "Metatag with id '$id' and value '$value' found.");
  }

  /**
   * Asserts user was redirected to a specific url.
   */
  public function assertRedirectedTo(string $url) {
    $status_code = $this->getSession()->getStatusCode();
    $this->assertTrue($status_code >= 300 && $status_code < 400, "Response for '$url' is a 3XX code.");
    $this->assertSession()->addressEquals($url, "Redirected to '$url'.");
  }

  /**
   * Asserts current page url is equals to url provided.
   */
  public function assertUrlIs(string $url) {
    $this->assertSession()->addressEquals($url, "Current url is '$url'.");
  }

  /**
   * Assert status code with custom message string.
   */
  public function assertStatusCode(int $code, string $message = '') {
    $actual = $this->getSession()->getStatusCode();
    if (empty($message)) {
      $message = sprintf('Current response status code is %d, but %d expected.', $actual, $code);
    }

    $this->assertEquals($code, intval($actual), $message);
  }

  /**
   * Similar to assertPageTextContains but displays page content when fails.
   */
  public function assertPageContains(string $text) {
    $this->assertStringContainsString($text, $this->getSession()->getPage()->getHtml());
  }

  /**
   * Similar to assertPageTextNotContains but displays page content when fails.
   */
  public function assertPageNotContains(string $text) {
    $this->assertStringNotContainsString($text, $this->getSession()->getPage()->getHtml());
  }

  /**
   * Executes some callback and tries the execution in case of an exception.
   */
  public function tryOrRepeat(callable $callback, $args = []) {
    $attempts = 0;
    do {
      try {
        call_user_func_array($callback, $args);
      }
      catch (\Exception $e) {
        if ($attempts++ > 5) {
          throw $e;
        }
        sleep(2);
        continue;
      }
      break;
    } while (TRUE);
  }

}
