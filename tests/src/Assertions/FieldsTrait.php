<?php

namespace Drupal\Tests\drupal_test_assertions\Assertions;

/**
 * Helper trait to perform assertions related to Field module.
 */
trait FieldsTrait {

  /**
   * Asserts an entity type has a field.
   */
  public function assertEntityTypeHasField(string $field_name, string $entity_type, string $bundle) {
    $id = "$entity_type.$bundle.$field_name";
    $config = \Drupal::entityTypeManager()->getStorage('field_config')->load($id);
    $this->assertTrue(!empty($config), "$entity_type ($bundle): has a '$field_name' field.");
  }

  /**
   * Asserts a field is hidden for a given view mode.
   */
  public function assertFieldIsHiddenInForm(string $field_name, string $entity_type, string $bundle, string $form_mode = 'default') {
    $form_display = \Drupal::service('entity_display.repository')->getFormDisplay($entity_type, $bundle, $form_mode);
    $this->assertNull($form_display->getComponent($field_name), "Field '$field_name' is hidden for $entity_type ($bundle).");
  }

  /**
   * Asserts a field is visible for a given view mode.
   */
  public function assertFieldIsVisibleInForm(string $field_name, string $entity_type, string $bundle, string $form_mode = 'default') {
    $form_display = \Drupal::service('entity_display.repository')->getFormDisplay($entity_type, $bundle, $form_mode);
    $this->assertNotNull($form_display->getComponent($field_name), "Field '$field_name' is hidden for $entity_type ($bundle).");
  }

  /**
   * Asserts a field is required when creating or editing an entity.
   */
  public function assertFieldIsRequired(string $field_name, string $entity_type, string $bundle) {
    $id = "$entity_type.$bundle.$field_name";
    $config = \Drupal::entityTypeManager()->getStorage('field_config')->load($id);
    $this->assertTrue($config->get('required'), "$entity_type ($bundle): has a '$field_name' is required.");
  }

  /**
   * Asserts a field is not required when creating or editing an entity.
   */
  public function assertFieldIsNotRequired(string $field_name, string $entity_type, string $bundle) {
    $id = "$entity_type.$bundle.$field_name";
    $config = \Drupal::entityTypeManager()->getStorage('field_config')->load($id);
    $this->assertFalse($config->get('required'), "$entity_type ($bundle): has a '$field_name' is required.");
  }

  /**
   * Assert an entity reference field can reference specific target_bundles.
   */
  public function assertEntityReferenceTargetBundles(array $expected_bundles, string $field_name, string $entity_type, string $bundle) {
    $id = "$entity_type.$bundle.$field_name";
    $config = \Drupal::entityTypeManager()->getStorage('field_config')->load($id);
    $target_bundles = $config->get('settings')['handler_settings']['target_bundles'];

    foreach ($expected_bundles as $expected_bundle) {
      $this->assertContains($expected_bundle, $target_bundles, "$field_name of $entity_type ($bundle) can reference $expected_bundle");
    }
  }

  /**
   * Assert an entity reference field can reference only the specified bundles.
   */
  public function assertEntityReferenceTargetBundlesStrict(array $expected_bundles, string $field_name, string $entity_type, string $bundle) {
    $id = "$entity_type.$bundle.$field_name";
    $config = \Drupal::entityTypeManager()->getStorage('field_config')->load($id);
    $target_bundles = $config->get('settings')['handler_settings']['target_bundles'];

    foreach ($expected_bundles as $expected_bundle) {
      $this->assertContains($expected_bundle, $target_bundles, "$field_name of $entity_type ($bundle) can reference $expected_bundle");
    }
    foreach ($target_bundles as $target_bundle) {
      $this->assertContains($target_bundle, $expected_bundles, "$field_name of $entity_type ($bundle) can reference $target_bundle but it should not.");
    }
  }

}
