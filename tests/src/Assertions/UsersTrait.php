<?php

namespace Drupal\Tests\drupal_test_assertions\Assertions;

use Drupal\user\UserInterface;
use Drupal\user\Entity\Role;

/**
 * Helper trait to perform assertions related to User module.
 */
trait UsersTrait {

  /**
   * Assers anonymous users cannot create new accounts in the site.
   */
  public function assertNoCreateAccountsAllowed() {
    $register_method = \Drupal::config('user.settings')->get('register');
    $this->assertTrue($register_method === UserInterface::REGISTER_ADMINISTRATORS_ONLY, "Anonymous users cannot create new accounts in the site");
  }

  /**
   * Asserts that risky permissions are not available for untrusted users.
   */
  public function assertUnprivilegedRolesCannotPerformRiskyActions() {
    $permissions = \Drupal::service('user.permissions')->getPermissions();
    $anonymous = Role::load('anonymous');
    $authenticated = Role::load('authenticated');
    foreach ($permissions as $permission_key => $permission) {
      if (empty($permission['restrict access'])) {
        continue;
      }

      $this->assertFalse($anonymous->hasPermission($permission_key), "Anonymous users should not have $permission_key.");
      $this->assertFalse($authenticated->hasPermission($permission_key), "Authenticated users should not have $permission_key.");
    }
  }

}
