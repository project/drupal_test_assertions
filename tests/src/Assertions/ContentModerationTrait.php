<?php

namespace Drupal\Tests\drupal_test_assertions\Assertions;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\workflows\Entity\Workflow;

/**
 * Helper trait to perform assertions related to Content Moderation module.
 */
trait ContentModerationTrait {

  /**
   * Asserts an entity type of a given bundle should be moderated.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type expected to be part of the moderation workflow.
   * @param string $bundle
   *   The entity type bundle.
   */
  public function assertModerationEnabledForEntityType(EntityTypeInterface $entity_type, string $bundle) {
    $service = \Drupal::service('content_moderation.moderation_information');
    $entity_type_id = $entity_type->id();
    $this->assertTrue($service->shouldModerateEntitiesOfBundle($entity_type, $bundle), "$entity_type_id ($bundle) should be moderated.");
  }

  /**
   * Asserts the configured workflow for an entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type part of the workflow.
   * @param string $bundle
   *   The entity type bundle part of the workflow.
   * @param string $workflow_name
   *   The name of the worfklow.
   */
  public function assertWorkflowForEntityType(EntityTypeInterface $entity_type, string $bundle, string $workflow_name = 'default') {
    $service = \Drupal::service('content_moderation.moderation_information');
    /** @var \Drupal\workflows\Entity\Workflow $workflow */
    $workflow = $service->getWorkflowForEntityTypeAndBundle($entity_type->id(), $bundle);
    $entity_type_id = $entity_type->id();
    $actual_workflow_name = $workflow->id();

    $error = "$entity_type_id ($bundle) should use the workflow $actual_workflow_name";
    $this->assertTrue(!empty($workflow) && $workflow_name === $actual_workflow_name, $error);
  }

  /**
   * Asserts a transition from a state to another state exists for a workflow.
   *
   * @param string $workflow_name
   *   The workflow name to check.
   * @param string $from
   *   The name of the initial the state.
   * @param string $to
   *   The name of the final step.
   */
  public function assertWorkflowHasTransition(string $workflow_name, string $from, string $to) {
    /** @var \Drupal\workflows\Entity\Workflow $workflow */
    $workflow = Workflow::load($workflow_name);
    $error = "Workflow '$workflow_name' has a transition from '$from' to '$to'";
    $this->assertTrue($workflow->getTypePlugin()->hasTransitionFromStateToState($from, $to), $error);
  }

  /**
   * Asserts a workflow transition from a state to another state not exists.
   *
   * @param string $workflow_name
   *   The workflow name to check.
   * @param string $from
   *   The name of the initial the state.
   * @param string $to
   *   The name of the final step.
   */
  public function assertWorkflowNotHasTransition(string $workflow_name, string $from, string $to) {
    /** @var \Drupal\workflows\Entity\Workflow $workflow */
    $workflow = Workflow::load($workflow_name);
    $error = "Workflow '$workflow_name' has a transition from '$from' to '$to'";
    $this->assertFalse($workflow->getTypePlugin()->hasTransitionFromStateToState($from, $to), $error);
  }

}
