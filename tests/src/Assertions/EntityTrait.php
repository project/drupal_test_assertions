<?php

namespace Drupal\Tests\drupal_test_assertions\Assertions;

/**
 * Helper trait to perform assertions related to entities.
 */
trait EntityTrait {

  /**
   * Asserts an entity with a specific bundle exists.
   */
  public function assertEntityExists(string $entity_type, string $bundle = NULL) {
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type);

    $bundle = $bundle ?? $entity_type;
    $this->assertTrue(!empty($bundles[$bundle]), "Entity $entity_type with bundle $bundle exists.");
  }

}
