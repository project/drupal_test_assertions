<?php

namespace Drupal\Tests\drupal_test_assertions\Assertions;

use Drupal\user\Entity\Role;
use Drupal\Core\Session\AccountInterface;

/**
 * Helper trait to perform assertions related roles.
 */
trait RolesTrait {

  /**
   * Asserts a role exists.
   */
  public function assertRoleExists(string $role_name) {
    $this->assertIsObject(Role::load($role_name), "The role '$role_name' exists.");
  }

  /**
   * Asserts a role has a set of permissions.
   */
  public function assertRoleHasPermissions(string $role_name, array $permissions) {
    $role = Role::load($role_name);
    $authenticated = Role::load(AccountInterface::AUTHENTICATED_ROLE);
    foreach ($permissions as $permission) {
      $this->assertTrue($role->hasPermission($permission) || $authenticated->hasPermission($permission), "The role '$role_name' has the '$permission' permission.");
    }
  }

}
