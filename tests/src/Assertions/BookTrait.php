<?php

namespace Drupal\Tests\drupal_test_assertions\Assertions;

/**
 * Helper trait to perform assertions related to Book module.
 */
trait BookTrait {

  /**
   * Asserts only specied node types are allowed in book outlines.
   *
   * @param array $node_types
   *   The enabled node types to check.
   */
  public function assertBooksAllowedInOutlines(array $node_types) {
    $allowed_types = \Drupal::config('book.settings')->get('allowed_types');
    $this->assertEquals($node_types, $allowed_types);
  }

  /**
   * Asserts only specied node type is configured to be child page.
   *
   * @param string $node_type
   *   The enabled node type to check.
   */
  public function assertBookChildPages(string $node_type) {
    $child_type = \Drupal::config('book.settings')->get('child_type');
    $this->assertEquals($node_type, $child_type);
  }

}
